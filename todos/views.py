from django.shortcuts import render, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm
# Create your views here.


def todo_list_list(request):
    todolist_list = TodoList.objects.all()
    context = { 
        "todolist_list": todolist_list, 
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    detail = TodoList.objects.get(id=id)
    context = {
        "detail": detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("todo_list_detail", id=form.instance.id)
    else:
        form = TodoListForm()
        context = {
            "form": form,
        }
        return render(request, "todos/create.html", context)
    

def todo_list_update(request, id):
    update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=update)
        if form.is_valid():
            # to redirect to the detail view of the model use this:
            updated = form.save()
            return redirect("todo_list_detail", id=updated.id)
    else:
        form = TodoListForm(instance=update)
    context = {
        
        "form": form,
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("todo_list_list")
    else:
        context = {
            "delete": delete,
        }
        return render(request, "todos/delete.html", context)
    

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("todo_list_detail", id=form.instance.list.id)
    else:
        form = TodoItemForm()
        context = {
            "form": form,
        }
    return render(request, "todos/item_create.html", context)


def todo_item_update(request, id):
    update = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=update)
        if form.is_valid():
            form.save() 
            return redirect("todo_list_detail", id=update.list.id)
    else:
        form = TodoItemForm(instance=update)
    context = {
        "form": form,
    }
    return render(request, "todos/item_update.html", context)